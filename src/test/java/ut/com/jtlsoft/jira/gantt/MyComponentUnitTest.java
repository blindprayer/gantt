package ut.com.jtlsoft.jira.gantt;

import org.junit.Test;
import com.jtlsoft.jira.gantt.api.MyPluginComponent;
import com.jtlsoft.jira.gantt.impl.MyPluginComponentImpl;

import static org.junit.Assert.assertEquals;

public class MyComponentUnitTest
{
    @Test
    public void testMyName()
    {
        MyPluginComponent component = new MyPluginComponentImpl(null);
        assertEquals("names do not match!", "myComponent",component.getName());
    }
}